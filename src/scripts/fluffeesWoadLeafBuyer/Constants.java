package scripts.fluffeesWoadLeafBuyer;

import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

public class Constants {

    public static final RSTile BANK_TILE = new RSTile(3012, 3355, 0);
    public static final RSTile WYSON_TILE = new RSTile(3028, 3379, 0);
    public static final RSArea FALADOR_WEST_BANK = new RSArea(new RSTile(3009, 3358, 0), new RSTile(3018, 3055, 0));
}
