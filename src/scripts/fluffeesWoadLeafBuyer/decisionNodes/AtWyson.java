package scripts.fluffeesWoadLeafBuyer.decisionNodes;

import scripts.fluffeesWoadLeafBuyer.Variables;
import org.tribot.api2007.NPCs;
import scripts.fluffeesAPI.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class AtWyson extends DecisionNode {

    @Override
    public boolean isValid() {
        return NPCs.find(Variables.getInstance().wyson.getFirstInteractableID()).length > 0 || NPCs.find(Variables.getInstance().wyson.getInteractableName()).length > 0;
    }

}
