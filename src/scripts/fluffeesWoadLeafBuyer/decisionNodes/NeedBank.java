package scripts.fluffeesWoadLeafBuyer.decisionNodes;

import scripts.fluffeesAPI.scripting.types.interactables.Interactable;
import scripts.fluffeesAPI.scripting.types.interactables.InteractableAction;
import scripts.fluffeesWoadLeafBuyer.Variables;
import org.tribot.api2007.Inventory;
import scripts.fluffeesAPI.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class NeedBank extends DecisionNode {

    @Override
    public boolean isValid() {
        if (Inventory.isFull()) {
            return true;
        }
        if (Variables.getInstance().coins.getFirstInteractableID() != Interactable.DEFAULT_ID) {
            return Inventory.getCount(Variables.getInstance().coins.getFirstInteractableID()) < 20;
        } else {
            return Inventory.getCount(Variables.getInstance().coins.getInteractableName()) < 20;
        }
    }

}
