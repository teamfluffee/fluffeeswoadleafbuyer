package scripts.fluffeesWoadLeafBuyer.decisionNodes;

import org.tribot.api2007.Banking;
import scripts.fluffeesAPI.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class BankOpen extends DecisionNode {

    @Override
    public boolean isValid() {
        return Banking.isBankScreenOpen();
    }

}
