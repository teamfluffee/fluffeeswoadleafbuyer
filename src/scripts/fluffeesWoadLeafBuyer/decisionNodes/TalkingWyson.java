package scripts.fluffeesWoadLeafBuyer.decisionNodes;

import scripts.fluffeesWoadLeafBuyer.Variables;
import org.tribot.api2007.NPCChat;
import scripts.fluffeesAPI.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;

public class TalkingWyson extends DecisionNode {

    @Override
    public boolean isValid() {
        return NPCChat.getName() != null && NPCChat.getName().equalsIgnoreCase(Variables.getInstance().wyson.getInteractableName());
    }

}
