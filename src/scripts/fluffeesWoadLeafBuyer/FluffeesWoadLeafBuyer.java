package scripts.fluffeesWoadLeafBuyer;

import org.tribot.api.General;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Painting;
import org.tribot.script.interfaces.Starting;
import scripts.fluffeesAPI.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionNode;
import scripts.fluffeesAPI.scripting.frameworks.decisionTree.decisionTreeFramework.DecisionTree;
import scripts.fluffeesAPI.scripting.frameworks.decisionTree.decisionTreeFramework.INode;
import scripts.fluffeesAPI.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesAPI.game.camera.ACamera;
import scripts.fluffeesAPI.scripting.painting.fluffeesPublicPaint.FluffeesPaint;
import scripts.fluffeesAPI.scripting.painting.fluffeesPublicPaint.PaintInfo;
import scripts.fluffeesAPI.utilities.Utilities;
import scripts.fluffeesWoadLeafBuyer.decisionNodes.*;
import scripts.fluffeesWoadLeafBuyer.processNodes.*;

import javax.swing.*;
import java.awt.*;

@ScriptManifest(authors = {"Fluffee"}, category = "Money making", name = "Fluffee's Woad Leaf Buyer", version = 1.00, description = "Buys Woad Leaves from Wyson the Gardener for profit." +
        "\n" + "<html><br/><font color='blue'>Version: 1.01</font></html>", gameMode = 1)

public class FluffeesWoadLeafBuyer extends Script implements Starting, PaintInfo, Painting {

    private double scriptVersion = getClass().getAnnotation(ScriptManifest.class).version();
    private final FluffeesPaint SCRIPT_PAINT = new FluffeesPaint(this, FluffeesPaint.PaintLocations.BOTTOM_RIGHT_PLAY_SCREEN, new Color[]{new Color(255, 251, 255)}, "Trebuchet MS", new Color[]{new Color(93, 156, 236, 127)},
            new Color[]{new Color(39, 95, 175)}, 1, false, 5, 3, 0);
    DecisionNode needBank = new NeedBank();
    DecisionNode atWyson = new AtWyson();
    DecisionNode atBank = new AtBank();
    DecisionNode talkingWyson = new TalkingWyson();
    DecisionNode bankOpen = new BankOpen();

    ProcessNode walkBank = new WalkBank();
    ProcessNode walkWyson = new WalkWyson();
    ProcessNode buyLeaves = new BuyLeaves();
    ProcessNode talkWyson = new TalkWyson();
    ProcessNode openBank = new OpenBank();
    ProcessNode bankItems = new BankItems();
    DecisionTree decisionTree;

    @Override
    public void onStart() {
        ACamera aCamera = new ACamera(this);
        Variables.getInstance().scriptStartTime = System.currentTimeMillis();
        Variables.getInstance().setACamera(aCamera);
    }

    @Override
    public void run() {
        GUI currentWindow = new GUI();
        currentWindow.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        currentWindow.setVisible(true);

        while (currentWindow.isVisible()) {
            General.sleep(500);
        }

        setupNodes();
        while(Variables.getInstance().runScript) {
            INode currentNode = decisionTree.getValidNode();
            if(currentNode != null) {
                Variables.getInstance().scriptStatus = currentNode.getStatus();
                currentNode.execute();
            } else {
                Variables.getInstance().runScript = false;
            }
        }
    }

    public void setupNodes() {
        needBank.addOnTrueNode(atBank);
        needBank.addOnFalseNode(atWyson);

        atWyson.addOnTrueNode(talkingWyson);
        atWyson.addOnFalseNode(walkWyson);

        talkingWyson.addOnTrueNode(buyLeaves);
        talkingWyson.addOnFalseNode(talkWyson);

        atBank.addOnTrueNode(bankOpen);
        atBank.addOnFalseNode(walkBank);

        bankOpen.addOnTrueNode(bankItems);
        bankOpen.addOnFalseNode(openBank);

        decisionTree = new DecisionTree(needBank);
    }

    @Override
    public void onPaint(Graphics graphics) {
        SCRIPT_PAINT.paint(graphics);
    }

    public JFrame findTRiBotFrame() {
        Frame[] frames = JFrame.getFrames();
        for (Frame tempFrame : frames) {
            if (tempFrame.getTitle().contains("TRiBot Old-School - The Desktop Botting Solution")) {
                return (JFrame) tempFrame;
            }
            General.sleep(100);
        }
        return null;
    }

    @Override
    public String[] getPaintInfo() {
        String[] paintInfoStrings = new String[4];
        paintInfoStrings[0] = "Fluffee's Woad Leaf Buyer v" + String.format("%.2f", scriptVersion);
        paintInfoStrings[1] = "Time ran: " + SCRIPT_PAINT.getRuntimeString();
        paintInfoStrings[2] = "Status: " + Variables.getInstance().scriptStatus;
        paintInfoStrings[3] = "Leaves Bought: " + Utilities.getAmountPerHour(Variables.getInstance().leavesBought, System.currentTimeMillis() - Variables.getInstance().scriptStartTime) + " P/H";
        return paintInfoStrings;
    }

}
