package scripts.fluffeesWoadLeafBuyer.processNodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;
import scripts.fluffeesAPI.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesAPI.scripting.types.interactables.Interactable;
import scripts.fluffeesAPI.scripting.types.interactables.InteractableAction;
import scripts.fluffeesAPI.utilities.Conditions;
import scripts.fluffeesWoadLeafBuyer.Variables;

public class BankItems extends ProcessNode {

    @Override
    public String getStatus() {
        return "banking Items";
    }

    @Override
    public void execute() {
        int currentCoinCount = 0;
        boolean depositAllExcept = false;
        if (Variables.getInstance().coins.getFirstInteractableID() != Interactable.DEFAULT_ID) {
            currentCoinCount = Inventory.getCount(Variables.getInstance().coins.getFirstInteractableID());
            if (currentCoinCount > 0) {
                depositAllExcept = true;
            }
        } else {
            currentCoinCount = Inventory.getCount(Variables.getInstance().coins.getFirstInteractableID());
            if (currentCoinCount > 0) {
                depositAllExcept = true;
            }
        }
        if (depositAllExcept) {
            if (Variables.getInstance().woadLeaves.getFirstInteractableID() != Interactable.DEFAULT_ID) {
                Banking.depositAllExcept(Variables.getInstance().woadLeaves.getFirstInteractableID());
            } else {
                Banking.depositAllExcept(Variables.getInstance().woadLeaves.getInteractableName());
            }
        } else {
            Banking.depositAll();
        }
        Timing.waitCondition(Conditions.inventoryCountChanged(Inventory.getAll().length), General.random(5000, 8000));
        if (currentCoinCount < Variables.getInstance().coinCount) {
            if (Variables.getInstance().coins.getFirstInteractableID() != Interactable.DEFAULT_ID) {
                Banking.withdraw(Variables.getInstance().coinCount, Variables.getInstance().coins.getFirstInteractableID());
                Timing.waitCondition(Conditions.withdrewItem(Variables.getInstance().coins.getFirstInteractableID(), currentCoinCount), General.random(3000, 6000));
            } else {
                Banking.withdraw(Variables.getInstance().coinCount, Variables.getInstance().coins.getInteractableName());
                Timing.waitCondition(Conditions.withdrewItem(Variables.getInstance().coins.getInteractableName(), currentCoinCount), General.random(3000, 6000));
            }
        }

    }
}
