package scripts.fluffeesWoadLeafBuyer.processNodes;

import scripts.fluffeesWoadLeafBuyer.Constants;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.WebWalking;
import scripts.fluffeesAPI.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesAPI.utilities.Conditions;

public class WalkBank extends ProcessNode {

    @Override
    public String getStatus() {
        return "Walking to Bank";
    }

    @Override
    public void execute() {
        WebWalking.walkTo(Constants.BANK_TILE);
        Timing.waitCondition(Conditions.nearTile(4, Constants.BANK_TILE), General.random(5000, 7000));
    }
}
