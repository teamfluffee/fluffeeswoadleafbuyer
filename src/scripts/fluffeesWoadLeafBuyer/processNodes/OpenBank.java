package scripts.fluffeesWoadLeafBuyer.processNodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Banking;
import scripts.fluffeesAPI.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesAPI.utilities.Conditions;

public class OpenBank extends ProcessNode {

    @Override
    public String getStatus() {
        return "Opening Bank";
    }

    @Override
    public void execute() {
        Banking.openBank();
        Timing.waitCondition(Conditions.bankOpened(), General.random(5000, 7000));
    }
}
