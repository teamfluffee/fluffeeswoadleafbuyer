package scripts.fluffeesWoadLeafBuyer.processNodes;

import org.tribot.api2007.Interfaces;
import org.tribot.api2007.Inventory;
import scripts.fluffeesWoadLeafBuyer.Variables;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.NPCChat;
import scripts.fluffeesAPI.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesAPI.scripting.antiban.AntiBan;
import scripts.fluffeesAPI.utilities.ChatUtilities;
import scripts.fluffeesAPI.utilities.Conditions;
import scripts.fluffeesAPI.utilities.Utilities;

public class BuyLeaves extends ProcessNode {
    @Override
    public String getStatus() {
        return "Buying Leaves";
    }

    @Override
    public void execute() {
        long waitTime = 0;
        int currentLeaves = Inventory.getCount(Variables.getInstance().woadLeaves.getInteractableName());
        while (NPCChat.getMessage() != null || NPCChat.getName() != null) {
            while (ChatUtilities.clickContinuePresent()) {
                if (!Variables.getInstance().spacebarForChat) {
                    AntiBan.generateTrackers(Variables.getInstance().chatInteractionABCSleep);
                    waitTime = System.currentTimeMillis();
                    ChatUtilities.clickContinue(2000);
                    waitTime = System.currentTimeMillis() - waitTime;
                    Variables.getInstance().chatInteractionABCCounter++;
                    Variables.getInstance().chatInteractionABCSleep = Utilities.addWaitTime((int) waitTime, Variables.getInstance().chatInteractionABCCounter, Variables.getInstance().chatInteractionABCSleep);
                    AntiBan.sleepReactionTime(Variables.getInstance().abcMultiplier);
                } else {
                    waitTime = System.currentTimeMillis();
                    ChatUtilities.holdSpaceForChat();
                    waitTime = System.currentTimeMillis() - waitTime;
                    Variables.getInstance().spaceChatABCCounter++;
                    Variables.getInstance().spaceChatABCSleep = Utilities.addWaitTime((int) waitTime, Variables.getInstance().spaceChatABCCounter, Variables.getInstance().spaceChatABCSleep);
                    AntiBan.sleepReactionTime(Variables.getInstance().abcMultiplier);
                }
            }
            if (optionsAvailable(General.random(1000, 3000))) {
                AntiBan.generateTrackers(Variables.getInstance().optionSelectABCSleep);
                waitTime = System.currentTimeMillis();
                ChatUtilities.handleChatOptions(Variables.getInstance().numbersForChat, "Yes please, I need woad leaves.", "How about 20 coins?");
                Timing.waitCondition(Conditions.optionsClosed(), General.random(5000, 8000));
                waitTime = System.currentTimeMillis() - waitTime;
                Variables.getInstance().optionSelectABCCounter++;
                Variables.getInstance().optionSelectABCSleep = Utilities.addWaitTime((int) waitTime, Variables.getInstance().optionSelectABCCounter, Variables.getInstance().optionSelectABCSleep);
                AntiBan.sleepReactionTime(Variables.getInstance().abcMultiplier);
            }
        }
        Variables.getInstance().leavesBought += Inventory.getCount(Variables.getInstance().woadLeaves.getInteractableName()) - currentLeaves;
    }

    public boolean optionsAvailable(int timeout) {
        int count = 2;
        while (timeout > 0 && count > 0 && !Interfaces.isInterfaceSubstantiated(219)) {
            int sleepTime = General.random(200, 400);
            timeout -= sleepTime;
            General.sleep(50, 150);
            if (!Interfaces.get(162, 45).isHidden() || Interfaces.isInterfaceSubstantiated(231) || Interfaces.isInterfaceSubstantiated(217))
                count--;
        }
        return Interfaces.isInterfaceSubstantiated(219);
    }
}
