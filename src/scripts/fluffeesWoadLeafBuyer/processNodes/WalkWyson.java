package scripts.fluffeesWoadLeafBuyer.processNodes;

import scripts.fluffeesWoadLeafBuyer.Constants;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.WebWalking;
import scripts.fluffeesAPI.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesAPI.utilities.Conditions;

public class WalkWyson extends ProcessNode {

    @Override
    public String getStatus() {
        return "Walking to Wyson";
    }

    @Override
    public void execute() {
        WebWalking.walkTo(Constants.WYSON_TILE);
        Timing.waitCondition(Conditions.nearTile(5, Constants.WYSON_TILE), General.random(5000, 7000));
    }
}