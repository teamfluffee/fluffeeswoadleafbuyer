package scripts.fluffeesWoadLeafBuyer.processNodes;

import scripts.fluffeesAPI.scripting.types.interactables.Interactable;
import scripts.fluffeesAPI.scripting.types.interactables.InteractableAction;
import scripts.fluffeesWoadLeafBuyer.Variables;
import scripts.fluffeesAPI.scripting.frameworks.decisionTree.decisionTreeFramework.ProcessNode;
import scripts.fluffeesAPI.utilities.npc.NPCInteraction;
import scripts.fluffeesAPI.scripting.antiban.AntiBan;
import scripts.fluffeesAPI.utilities.Utilities;

public class TalkWyson extends ProcessNode {

    @Override
    public String getStatus() {
        return "Talking to Wyson";
    }

    @Override
    public void execute() {
        AntiBan.generateTrackers(Variables.getInstance().wysonInteractionABCSleep);
        long waitTime;
        if (Variables.getInstance().wyson.getFirstInteractableID() != Interactable.DEFAULT_ID) {
            waitTime = System.currentTimeMillis();
            NPCInteraction.interactABC2("Talk-to", false, Variables.getInstance().wyson.getFirstInteractableID(), Variables.getInstance().aCamera);
        } else {
            waitTime = System.currentTimeMillis();
            NPCInteraction.interactABC2("Talk-to", false, Variables.getInstance().wyson.getInteractableName(), Variables.getInstance().aCamera);
        }
        waitTime = System.currentTimeMillis() - waitTime;
        Variables.getInstance().wysonInteractionABCCounter++;
        Variables.getInstance().wysonInteractionABCSleep = Utilities.addWaitTime((int) waitTime, Variables.getInstance().wysonInteractionABCCounter, Variables.getInstance().wysonInteractionABCSleep);
        AntiBan.sleepReactionTime(Variables.getInstance().abcMultiplier);
    }
}