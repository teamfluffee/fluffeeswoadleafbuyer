package scripts.fluffeesWoadLeafBuyer;

import scripts.fluffeesAPI.game.camera.ACamera;
import scripts.fluffeesAPI.scripting.types.interactables.Interactable;
import scripts.fluffeesAPI.scripting.types.interactables.InteractableAction;

public class Variables {

    private static Variables instance = new Variables();

    private Variables(){}

    public static Variables getInstance(){
        return instance;
    }

    public InteractableAction wyson = new InteractableAction("Wyson the gardener", Interactable.DEFAULT_ID, "Talk-to");
    public InteractableAction woadLeaves = new InteractableAction("Woad leaf", Interactable.DEFAULT_ID, null);
    public InteractableAction coins = new InteractableAction("Coins", Interactable.DEFAULT_ID, null);

    public ACamera aCamera = new ACamera();

    public void setACamera(ACamera aCamera) {
        this.aCamera = aCamera;
    }

    public int wysonInteractionABCCounter = 1; //Starting values
    public int wysonInteractionABCSleep = 4000; //Starting values

    public int spaceChatABCCounter = 1;
    public int spaceChatABCSleep = 800;

    public int chatInteractionABCCounter = 1;
    public int chatInteractionABCSleep = 300;

    public int optionSelectABCCounter = 1;
    public int optionSelectABCSleep = 500;

    public long scriptStartTime = 0;
    public String scriptStatus = "Nothing";
    public boolean runScript = true;
    public boolean spacebarForChat = true;
    public boolean numbersForChat = false;
    public double abcMultiplier = 1.0;
    public int coinCount = 10000;
    public int leavesBought = 0;

}
