package scripts.fluffeesWoadLeafBuyer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUI extends JFrame {

    private JPanel contentPane;
    private JTextField txtCoins;
    private int coins;
    private double abcMultiplier;
    private boolean spacebarChat, numbersChat;


    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    GUI frame = new GUI();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public GUI() {

        setTitle("Fluffee's Woad Leaf Buyer");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 340, 280);
        setMinimumSize(new Dimension(340, 280));
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JPanel pnlTitle = new JPanel();
        contentPane.add(pnlTitle, BorderLayout.NORTH);

        JLabel lblTitle = new JLabel("<html><font color=\"#5D9CEC\">Fluffee's</font> Woad Leaf Buyer</html>");
        lblTitle.setFont(new Font("Trebuchet MS", Font.PLAIN, 24));
        Border border = BorderFactory.createMatteBorder(0, 0, 1, 0,Color.BLACK);
        lblTitle.setBorder(border);
        pnlTitle.add(lblTitle);

        JPanel pnlOptions = new JPanel();
        contentPane.add(pnlOptions, BorderLayout.CENTER);
        GridBagLayout gbl_pnlOptions = new GridBagLayout();
        gbl_pnlOptions.columnWidths = new int[]{116, 30, 147, 0};
        gbl_pnlOptions.rowHeights = new int[]{19, 23, 0, 64, 25, 0};
        gbl_pnlOptions.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
        gbl_pnlOptions.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
        pnlOptions.setLayout(gbl_pnlOptions);

        JPanel pnlCoins = new JPanel();
        GridBagConstraints gbc_panel_1 = new GridBagConstraints();
        gbc_panel_1.gridwidth = 3;
        gbc_panel_1.insets = new Insets(0, 0, 5, 5);
        gbc_panel_1.fill = GridBagConstraints.HORIZONTAL;
        gbc_panel_1.gridx = 0;
        gbc_panel_1.gridy = 0;
        pnlOptions.add(pnlCoins, gbc_panel_1);

        JLabel lblCoins = new JLabel("Coins: ");
        pnlCoins.add(lblCoins);

        txtCoins = new JTextField();
        pnlCoins.add(txtCoins);
        txtCoins.setColumns(10);

        JRadioButton rdbtnSpacebarForChat = new JRadioButton("Spacebar for chat");
        rdbtnSpacebarForChat.setHorizontalAlignment(SwingConstants.CENTER);
        GridBagConstraints gbc_rdbtnSpacebarForChat = new GridBagConstraints();
        gbc_rdbtnSpacebarForChat.fill = GridBagConstraints.HORIZONTAL;
        gbc_rdbtnSpacebarForChat.insets = new Insets(0, 0, 5, 0);
        gbc_rdbtnSpacebarForChat.gridwidth = 3;
        gbc_rdbtnSpacebarForChat.gridx = 0;
        gbc_rdbtnSpacebarForChat.gridy = 1;
        pnlOptions.add(rdbtnSpacebarForChat, gbc_rdbtnSpacebarForChat);
        java.util.Hashtable<Integer,JLabel> labelTable = new java.util.Hashtable<Integer,JLabel>();
        labelTable.put(new Integer(20), new JLabel("2.0"));
        labelTable.put(new Integer(15), new JLabel("1.5"));
        labelTable.put(new Integer(10), new JLabel("1.0"));
        labelTable.put(new Integer(5), new JLabel("0.5"));
        labelTable.put(new Integer(0), new JLabel("0.0"));

        JSlider sldABCMultiplier = new JSlider();
        sldABCMultiplier.setMajorTickSpacing(1);
        sldABCMultiplier.setValue(10);
        sldABCMultiplier.setLabelTable( labelTable );

        JRadioButton rdbtnNumbersForChat = new JRadioButton("Numbers for chat");
        rdbtnNumbersForChat.setHorizontalAlignment(SwingConstants.CENTER);
        GridBagConstraints gbc_rdbtnNumbersForChat = new GridBagConstraints();
        gbc_rdbtnNumbersForChat.fill = GridBagConstraints.HORIZONTAL;
        gbc_rdbtnNumbersForChat.gridwidth = 3;
        gbc_rdbtnNumbersForChat.insets = new Insets(0, 0, 5, 0);
        gbc_rdbtnNumbersForChat.gridx = 0;
        gbc_rdbtnNumbersForChat.gridy = 2;
        pnlOptions.add(rdbtnNumbersForChat, gbc_rdbtnNumbersForChat);
        sldABCMultiplier.setPaintLabels(true);
        sldABCMultiplier.setPaintTicks(true);
        sldABCMultiplier.setSnapToTicks(true);
        sldABCMultiplier.setBorder(new TitledBorder(null, "ABC2 Multiplier", TitledBorder.CENTER, TitledBorder.TOP, null, null));
        sldABCMultiplier.setMaximum(20);
        GridBagConstraints gbc_sldABCMultiplier = new GridBagConstraints();
        gbc_sldABCMultiplier.fill = GridBagConstraints.HORIZONTAL;
        gbc_sldABCMultiplier.gridwidth = 3;
        gbc_sldABCMultiplier.insets = new Insets(0, 0, 5, 0);
        gbc_sldABCMultiplier.gridx = 0;
        gbc_sldABCMultiplier.gridy = 3;
        pnlOptions.add(sldABCMultiplier, gbc_sldABCMultiplier);

        JButton btnStartScript = new JButton("Start Script!");
        btnStartScript.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    coins = Integer.parseInt(txtCoins.getText());
                } catch (NumberFormatException error) {
                    System.out.println("Error: That World value is not an integer.");
                    txtCoins.setBorder(BorderFactory.createLineBorder(Color.red));
                    return;
                }
                Variables.getInstance().coinCount = coins;
                Variables.getInstance().spacebarForChat = rdbtnSpacebarForChat.isSelected();
                Variables.getInstance().numbersForChat = rdbtnNumbersForChat.isSelected();
                Variables.getInstance().abcMultiplier = (double) sldABCMultiplier.getValue()/10;
                dispose();
            }
        });
        GridBagConstraints gbc_btnStartScript = new GridBagConstraints();
        gbc_btnStartScript.gridwidth = 3;
        gbc_btnStartScript.gridx = 0;
        gbc_btnStartScript.gridy = 4;
        pnlOptions.add(btnStartScript, gbc_btnStartScript);
    }

}

